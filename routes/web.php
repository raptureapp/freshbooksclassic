<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\FreshbooksClassic\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::get('freshbooks/import', 'ImportController@index')->name('freshbooks.import');
        Route::get('freshbooks/sales', 'ClientSalesController@index')->name('freshbooks.sales');
        Route::get('freshbooks/category', 'SalesByCategoryController@index')->name('freshbooks.category');
        Route::get('freshbooks/expenses', 'ExpensesController@index')->name('freshbooks.expenses');
    });

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\FreshbooksClassic\Controllers\Imports')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::get('freshbooks/import/categories', 'CategoriesController@store')->name('import.categories');
        Route::get('freshbooks/import/clients', 'ClientsController@store')->name('import.clients');
        Route::get('freshbooks/import/invoices', 'InvoicesController@store')->name('import.invoices');
        Route::get('freshbooks/import/expenses', 'ExpensesController@store')->name('import.expenses');
    });
