@extends('rapture::layouts.dashboard')

@section('title', __('freshbooks::package.sales'))

@section('content')
    @heading
        @lang('freshbooks::package.plural')

        @slot('after')
            <ul class="sales-filters">
                <li><a href="{{ route('dashboard.freshbooks.sales') }}?type=alpha">Alphabetically</a></li>
                <li class="active"><a href="{{ route('dashboard.freshbooks.sales') }}?type=manager">By Manager</a></li>
            </ul>
        @endslot
    @endheading

    @statuses

    <div class="container">
        @foreach ($users as $user)
            <h3>{{ $user->name }}</h3>

            <table class="table striped">
                <tr>
                    <th>@lang('rapture::field.name')</th>
                    @foreach ($months as $name)
                        <th>{!! $name !!}</th>
                    @endforeach
                </tr>
                @foreach ($invoices->get($user->id) as $group)
                    <tr>
                        <td>{{ $clients[$group['client_id']] }}</td>
                        @foreach ($months as $key => $name)
                            <td>{{ $group['items']->has($key) ? '$' . number_format($group['items']->get($key)->sum('total') / 100, 2) : '--' }}</td>
                        @endforeach
                    </tr>
                @endforeach
                <tr>
                    <td><strong>Total</strong></td>
                    @foreach ($months as $key => $name)
                        <td><strong>{{ $totals->get($user->id)->has($key) ? '$' . number_format($totals->get($user->id)->get($key)->sum('total') / 100, 2) : '--' }}</strong></td>
                    @endforeach
                </tr>
            </table>
        @endforeach
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('freshbooks/css/style.css') }}">
@endpush
