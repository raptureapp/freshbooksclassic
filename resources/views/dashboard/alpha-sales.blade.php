@extends('rapture::layouts.dashboard')

@section('title', __('freshbooks::package.sales'))

@section('content')
    @heading
        @lang('freshbooks::package.plural')

        @slot('after')
            <ul class="sales-filters">
                <li class="active"><a href="{{ route('dashboard.freshbooks.sales') }}?type=alpha">Alphabetically</a></li>
                <li><a href="{{ route('dashboard.freshbooks.sales') }}?type=manager">By Manager</a></li>
            </ul>
        @endslot
    @endheading

    @statuses

    <div class="container">
        <!-- <div>Display mode (Month / Quarter / Year / Custom)</div> -->
        <!-- <div>Filters (Spend / Category)</div> -->

        <div class="flex filter-box">
            <form action="" method="get" class="flex spending">
                <select name="outcome">
                    <option value="over">Spent Over</option>
                    <!-- <option value="under">Spent Under</option> -->
                    <!-- <option value="">Spends Between</option> -->
                </select>
                <input type="text" name="spend">
                <select name="term">
                    <option value="month">Per Month</option>
                    <option value="year">Per Year</option>
                    <option value="overall">Overall</option>
                </select>
                <button type="submit" class="btn muted">Filter</button>
            </form>

            <form action="" method="get" class="flex date-range">
                <datepicker name="start" value=""></datepicker>
                <em class="far fa-arrows-h"></em>
                <datepicker name="end" value=""></datepicker>
                <button type="submit" class="btn muted">Filter</button>
            </form>
        </div>

        <table class="table striped">
            <tr>
                <th>@lang('rapture::field.name')</th>
                @foreach ($intervals as $name)
                    <th>{!! $name !!}</th>
                @endforeach
            </tr>
            @foreach ($invoices as $client_id => $invoice)
                <tr>
                    <td>{{ $clients[$client_id] }}</td>
                    @foreach ($intervals as $key => $name)
                        <td>{{ $invoice->has($key) ? '$' . number_format($invoice->get($key) / 100, 2) : '--' }}</td>
                    @endforeach
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@resource('datepicker')

@push('styles')
    <link rel="stylesheet" href="{{ asset('freshbooks/css/style.css') }}">
@endpush
