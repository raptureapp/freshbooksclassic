@extends('rapture::layouts.dashboard')

@section('title', 'Sales by Department')

@section('content')
    @heading
        Sales by Department
    @endheading

    @statuses

    <div class="container">
        <table class="table striped">
            <tr>
                <th></th>
                <th>Print</th>
                <th>Graphics</th>
                <th>Web</th>
                <th>Social</th>
                <th>SEM</th>
                <th>Other</th>
            </tr>
            @foreach ($lineItems as $key => $totals)
                <tr>
                    <td>{{ $key }}</td>
                    <td>${{ number_format($totals['print'] / 100) }}</td>
                    <td>${{ number_format($totals['graphics'] / 100) }}</td>
                    <td>${{ number_format($totals['web'] / 100) }}</td>
                    <td>${{ number_format($totals['social'] / 100) }}</td>
                    <td>${{ number_format($totals['sem'] / 100) }}</td>
                    <td>${{ number_format($totals['other'] / 100) }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('freshbooks/css/style.css') }}">
@endpush
