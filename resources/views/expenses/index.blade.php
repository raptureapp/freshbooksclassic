@extends('rapture::layouts.dashboard')

@section('title', 'Expenses')

@section('content')
    @heading
        Expenses
    @endheading

    @statuses

    <div class="container">
        <table class="table striped">
            <tr>
                <th></th>
                <th>Expenses</th>
            </tr>
            @foreach ($expenses as $key => $group)
                <tr>
                    <td>{{ $key }}</td>
                    <td>
                        @foreach ($group as $label => $amount)
                            @if (!$loop->first)
                                <br>
                            @endif
                            {{ $label }}: ${{ number_format($amount / 100) }}
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('freshbooks/css/style.css') }}">
@endpush
