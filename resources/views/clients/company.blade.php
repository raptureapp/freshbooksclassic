<div class="form-field">
    <label for="client-address">Address</label>
    <input type="email" name="address" id="client-address" value="{{ old('address') }}">
</div>
<div class="row form-field">
    <div class="w6">
        <label for="client-city">City</label>
        <input type="text" name="city" id="client-city" value="{{ old('city') }}">
    </div>
    <div class="w3">
        <label for="client-province">Province</label>
        <input type="text" name="region" id="client-province" value="{{ old('region') }}">
    </div>
    <div class="w3">
        <label for="client-postal">Postal Code</label>
        <input type="text" name="postal" id="client-postal" value="{{ old('postal') }}">
    </div>
</div>
