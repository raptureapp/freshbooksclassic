<div class="widget">
    <div class="form-field">
        <label><em class="far fa-link"></em>Freshbooks Account</label>
        <select name="freshbooks_id" id="freshbooksId">
            <option value="">- Disconnected -</option>
            @foreach ($accounts as $account)
            <option value="{{ $account->id }}"{{ $client->id === $account->client_id ? ' selected' : '' }}>{{ $account->name }}</option>
            @endforeach
        </select>
    </div>
</div>
