@extends('rapture::layouts.dashboard')

@section('title', __('freshbooks::package.import'))

@section('content')
    @heading
        @lang('freshbooks::package.import')

        @slot('after')
            Last ran: {{ is_null($lastRun) ? 'Never' : $lastRun->format('Y/m/d g:i a') }}
        @endslot
    @endheading

    @statuses

    <div class="container">
        @if (is_null($lastRun))
        <freshbooks-import></freshbooks-import>
        @else
        <freshbooks-import since="{{ $lastRun->format('Y-m-d') }}"></freshbooks-import>
        @endif
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('freshbooks/css/style.css') }}">
@endpush

@push('components')
    <script src="{{ asset('freshbooks/js/freshbooks.js') }}"></script>
@endpush
