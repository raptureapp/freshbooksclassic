<?php

return [
    'singular' => 'Freshbooks',
    'plural' => 'Freshbooks',
    'sales' => 'Sales',
    'import' => 'Import',
];
