/* global Vue */
import FreshbooksImport from './components/FreshbooksImport.vue';

Vue.component('freshbooks-import', FreshbooksImport);
