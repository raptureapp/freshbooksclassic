const { setup, styles, javascript, browsersync, monitor } = require('argonauts');
const { parallel, series } = require('gulp');
const { VueLoaderPlugin } = require('vue-loader');

setup({
    url: 'homestead.test',
    entries: ['freshbooks'],
    dest: 'public',
    src: 'resources/assets',
    jsExt: '{js,vue}',
    watch: [
        '*.php',
        '**/*.php',
    ],
    webpack: {
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    exclude: /node_modules/,
                    use: 'vue-loader',
                },
            ],
        },
        plugins: [
            new VueLoaderPlugin(),
        ],
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
            },
        },
    },
});

exports.default = series(browsersync, parallel(styles, javascript), monitor);
