<?php

namespace Rapture\FreshbooksClassic\Models;

use Illuminate\Database\Eloquent\Model;

class FreshbooksCategory extends Model
{
    protected $guarded = [];
}
