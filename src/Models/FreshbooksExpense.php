<?php

namespace Rapture\FreshbooksClassic\Models;

use Illuminate\Database\Eloquent\Model;

class FreshbooksExpense extends Model
{
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'posted_date',
    ];

    public function category()
    {
        return $this->belongsTo(FreshbooksCategory::class, 'freshbooks_category_id');
    }
}
