<?php

namespace Rapture\FreshbooksClassic\Models;

use Illuminate\Database\Eloquent\Model;

class FreshbooksLineItem extends Model
{
    protected $guarded = [];

    public function invoice()
    {
        return $this->belongsTo(FreshbooksInvoice::class, 'freshbooks_invoice_id');
    }
}
