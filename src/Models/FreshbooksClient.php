<?php

namespace Rapture\FreshbooksClassic\Models;

use Illuminate\Database\Eloquent\Model;

class FreshbooksClient extends Model
{
    protected $guarded = [];
}
