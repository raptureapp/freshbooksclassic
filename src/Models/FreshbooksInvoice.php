<?php

namespace Rapture\FreshbooksClassic\Models;

use Illuminate\Database\Eloquent\Model;
use Rapture\Clients\Models\Client;

class FreshbooksInvoice extends Model
{
    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'invoice_date',
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function lines()
    {
        return $this->hasMany(FreshbooksLineItem::class);
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->whereBetween('invoice_date', [$start, $end]);
    }
}
