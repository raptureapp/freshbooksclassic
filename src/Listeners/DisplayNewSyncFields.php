<?php

namespace Rapture\FreshbooksClassic\Listeners;

use Rapture\FreshbooksClassic\Models\FreshbooksClient;

class DisplayNewSyncFields
{
    public function handle($data)
    {
        $accounts = FreshbooksClient::orderBy('name')->get();

        echo view('freshbooks::clients.create', [
            'accounts' => $accounts,
        ]);
    }
}
