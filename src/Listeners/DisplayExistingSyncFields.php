<?php

namespace Rapture\FreshbooksClassic\Listeners;

use Rapture\FreshbooksClassic\Models\FreshbooksClient;

class DisplayExistingSyncFields
{
    public function handle($data)
    {
        $accounts = FreshbooksClient::orderBy('name')->get();

        echo view('freshbooks::clients.edit', [
            'client' => $data['client'],
            'accounts' => $accounts,
        ]);
    }
}
