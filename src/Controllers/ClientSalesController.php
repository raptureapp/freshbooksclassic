<?php

namespace Rapture\FreshbooksClassic\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rapture\Clients\Models\Client;
use Rapture\FreshbooksClassic\Models\FreshbooksInvoice;

class ClientSalesController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('freshbooks.sales', 'freshbooks');

        $type = $request->query('type', 'alpha');

        switch ($type) {
            case 'alpha':
                return $this->alphaSorting($request);
            case 'manager':
                return $this->managerSorting($request);
        }
    }

    /**
     * @todo Convert to sub queries
     */
    private function alphaSorting(Request $request)
    {
        $start = Carbon::now()->startOfMonth()->subMonths(11)->startOfDay();
        $end = Carbon::now()->endOfMonth()->endOfDay();

        if ($request->filled('start')) {
            $start = Carbon::parse($request->query('start'));
        }

        if ($request->filled('end')) {
            $end = Carbon::parse($request->query('end'))->endOfDay();
        }

        $intervals = $this->groupByMonth($start, $end);
        $grouping = function ($invoice) {
            return $invoice->invoice_date->format('Y-m');
        };

        $filter = null;

        if ($request->filled('outcome')) {
            $amount = floatval($request->query('spend', 0)) * 100;

            switch ($request->query('term', 'overall')) {
                case 'month':
                    $term = $start->floatDiffInMonths($end, true);
                    break;
                case 'year':
                    $term = $start->floatDiffInYears($end, true);
                    break;
                default:
                    $term = 1;
            }

            $filter = function ($groups) use ($term, $amount) {
                return $groups->sum() / $term > $amount;
            };
        }

        $clients = Client::all()->mapWithKeys(function ($client) {
            return [$client->id => $client->name];
        })->toArray();

        $invoices = FreshbooksInvoice::between($start, $end)
            ->get()
            ->groupBy('client_id')
            ->mapWithKeys(function ($clientInvoices, $clientId) use ($grouping) {
                return [$clientId => $clientInvoices->groupBy($grouping)->map(function ($group) {
                    return $group->sum('total');
                })];
            })
            ->filter($filter)
            ->sortBy(function ($invoice, $clientId) use ($clients) {
                return strtolower($clients[$clientId]);
            });

        return view('freshbooks::dashboard.alpha-sales', compact('intervals', 'clients', 'invoices'));
    }

    private function groupByMonth($start, $end)
    {
        $beginning = $start->copy();

        $length = ceil($start->floatDiffInMonths($end, true));
        $intervals = collect();

        for ($i = 1; $i <= $length; $i++) {
            $intervals->put($beginning->format('Y-m'), $beginning->format('M') . '<br>' . $beginning->format('Y'));
            $beginning->addMonth();
        }

        return $intervals;
    }

    private function managerSorting(Request $request)
    {
        $yearAgo = Carbon::now()->startOfMonth()->subYear();
        $months = collect([
            $yearAgo->format('Y-m') => $yearAgo->format('M') . '<br>' . $yearAgo->format('Y'),
        ]);

        $clients = Client::all();

        $clientArray = $clients->mapWithKeys(function ($client) {
            return [$client->id => $client->name];
        })->toArray();

        $managerArray = $clients->reject(function ($client) {
            return is_null($client->manager_id);
        })->mapWithKeys(function ($client) {
            return [$client->id => $client->manager_id];
        })->toArray();

        $invoices = FreshbooksInvoice::whereDate('invoice_date', '>', $yearAgo)
            ->get();

        $usedManagerIds = $clients->pluck('manager_id')->reject(function ($id) {
            return is_null($id);
        })->unique()->values()->toArray();

        $users = User::find($usedManagerIds);

        $filteredInvoices = $invoices->groupBy('client_id')
            ->mapWithKeys(function ($clientInvoices, $clientId) {
                return [$clientId => [
                    'client_id' => $clientId,
                    'items' => $clientInvoices->groupBy(function ($invoice) {
                        return $invoice->invoice_date->format('Y-m');
                    }),
                ]];
            })
            ->sortBy(function ($invoice, $clientId) use ($clientArray) {
                return strtolower($clientArray[$clientId]);
            });

        for ($i = 1; $i < 12; $i++) {
            $yearAgo->addMonth();
            $months->put($yearAgo->format('Y-m'), $yearAgo->format('M') . '<br>' . $yearAgo->format('Y'));
        }

        $assignedInvoices = $filteredInvoices->filter(function ($group, $clientId) use ($managerArray) {
            return array_key_exists($clientId, $managerArray);
        })->groupBy(function ($group, $clientId) use ($managerArray) {
            return $managerArray[$clientId];
        });

        $managerTotals = $invoices->filter(function ($invoice) use ($managerArray) {
            return array_key_exists($invoice->client_id, $managerArray);
        })->groupBy(function ($invoice) use ($managerArray) {
            return $managerArray[$invoice->client_id];
        })->mapWithKeys(function ($clientInvoices, $managerId) {
            return [$managerId => $clientInvoices->groupBy(function ($invoice) {
                return $invoice->invoice_date->format('Y-m');
            })];
        });

        return view('freshbooks::dashboard.manager-sales', [
            'users' => $users,
            'months' => $months,
            'clients' => $clientArray,
            'invoices' => $assignedInvoices,
            'totals' => $managerTotals,
        ]);
    }
}
