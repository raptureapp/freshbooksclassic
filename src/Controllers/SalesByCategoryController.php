<?php

namespace Rapture\FreshbooksClassic\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\FreshbooksClassic\Helpers\SalesCategory;
use Rapture\FreshbooksClassic\Models\FreshbooksLineItem;

class SalesByCategoryController extends Controller
{
    public function index(Request $request)
    {
        $classifications = new SalesCategory();

        $lineItems = FreshbooksLineItem::with('invoice')->get()->groupBy(function ($item) {
            return $item->invoice->invoice_date->format('Y-m');
        })->map(function ($group) use ($classifications) {
            $totals = [
                'print' => 0,
                'graphics' => 0,
                'web' => 0,
                'social' => 0,
                'other' => 0,
                'sem' => 0,
            ];

            foreach ($group as $line) {
                if (!$classifications->qualify($line->sku)) {
                    echo $line->freshbooks_invoice_id . ' ' . $line->sku . '<br>';
                    continue;
                }

                $totals[$classifications->classify($line->sku)] += $line->amount;
            }

            return $totals;
        })->sortKeysDesc();

        return view('freshbooks::sales.index', compact('lineItems'));
    }
}
