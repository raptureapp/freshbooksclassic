<?php

namespace Rapture\FreshbooksClassic\Controllers\Imports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\FreshbooksClassic\Helpers\FreshbooksClassic;
use Rapture\FreshbooksClassic\Models\FreshbooksCategory;

class CategoriesController extends Controller
{
    public function store(Request $request)
    {
        $apiClient = new FreshbooksClassic();
        $categories = $apiClient->request('category.list', [
            'per_page' => 100,
            'page' => $request->query('page', 1),
        ]);

        if (is_null($categories)) {
            return response()->json(['status' => 'error']);
        }

        foreach ($categories->get('categories')->category as $category) {
            FreshbooksCategory::updateOrCreate([
                'ref' => intval($category->category_id),
            ], [
                'name' => $category->name,
            ]);
        }

        return response()->json($categories->attributes());
    }
}
