<?php

namespace Rapture\FreshbooksClassic\Controllers\Imports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\Clients\Models\Client;
use Rapture\Clients\Models\Contact;
use Rapture\FreshbooksClassic\Helpers\FreshbooksClassic;
use Rapture\FreshbooksClassic\Models\FreshbooksClient;

class ClientsController extends Controller
{
    public function store(Request $request)
    {
        $apiClient = new FreshbooksClassic();

        $apiRequest = [
            'per_page' => 100,
            'folder' => 'active',
            'page' => $request->query('page', 1),
        ];

        if ($request->filled('since')) {
            $apiRequest['updated_from'] = $request->query('since') . ' 00:00:00';
        }

        $clients = $apiClient->request('client.list', $apiRequest);
        $existingClients = FreshbooksClient::pluck('freshbooks_ref');

        if (is_null($clients)) {
            return response()->json(['status' => 'error']);
        }

        $attributes = $clients->attributes();

        if ($attributes->get('total') == 0) {
            return response()->json($attributes);
        }

        $clients = $clients->get('clients')->client;

        if ($attributes->get('total') == 1) {
            $clients = [$clients];
        }

        foreach ($clients as $client) {
            if ($existingClients->contains($client->client_id)) {
                continue;
            }

            $newClient = Client::create([
                'name' => $client->organization,
            ]);

            FreshbooksClient::create([
                'client_id' => $newClient->id,
                'freshbooks_ref' => $client->client_id,
                'name' => $client->organization,
                'statement_url' => $client->url,
                'account_url' => $client->auth_url,
            ]);

            $contact = collect([
                'client_id' => $newClient->id,
                'first_name' => !is_object($client->first_name) ? $client->first_name : 'Unknown',
                'last_name' => !is_object($client->last_name) ? $client->last_name : null,
                'email' => $client->email,
                'is_main' => true,
            ])->filter()->toArray();

            Contact::create($contact);
        }

        return response()->json($attributes);
    }
}
