<?php

namespace Rapture\FreshbooksClassic\Controllers\Imports;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rapture\FreshbooksClassic\Helpers\FreshbooksClassic;
use Rapture\FreshbooksClassic\Models\FreshbooksCategory;
use Rapture\FreshbooksClassic\Models\FreshbooksExpense;

class ExpensesController extends Controller
{
    public function store(Request $request)
    {
        $apiClient = new FreshbooksClassic();
        $apiRequest = [
            'per_page' => 100,
            'folder' => 'active',
            'page' => $request->query('page', 1),
        ];

        if ($request->filled('since')) {
            $apiRequest['date_from'] = $request->query('since') . ' 00:00:00';
        }

        $expenses = $apiClient->request('expense.list', $apiRequest);

        if (is_null($expenses)) {
            return response()->json(['status' => 'error']);
        }

        $attributes = $expenses->attributes();

        if ($attributes->get('total') == 0) {
            return response()->json($attributes);
        }

        $categories = FreshbooksCategory::get()->mapWithKeys(function ($category) {
            return [$category->ref => $category->id];
        })->toArray();

        $expenses = $expenses->get('expenses')->expense;

        if ($attributes->get('total') == 1) {
            $expenses = [$expenses];
        }

        foreach ($expenses as $expense) {
            if (!array_key_exists($expense->category_id, $categories)) {
                continue;
            }

            FreshbooksExpense::updateOrCreate([
                'ref' => $expense->expense_id,
            ], [
                'freshbooks_category_id' => $categories[$expense->category_id],
                'amount' => floatval($expense->amount) * 100,
                'posted_date' => $expense->date,
            ]);
        }

        return response()->json($attributes);
    }
}
