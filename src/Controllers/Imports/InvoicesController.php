<?php

namespace Rapture\FreshbooksClassic\Controllers\Imports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\FreshbooksClassic\Helpers\FreshbooksClassic;
use Rapture\FreshbooksClassic\Models\FreshbooksClient;
use Rapture\FreshbooksClassic\Models\FreshbooksInvoice;

class InvoicesController extends Controller
{
    public function store(Request $request)
    {
        $apiClient = new FreshbooksClassic();

        $apiRequest = [
            'per_page' => 100,
            'folder' => 'active',
            'page' => $request->query('page', 1),
        ];

        if ($request->filled('since')) {
            $apiRequest['updated_from'] = $request->query('since') . ' 00:00:00';
        }

        $invoices = $apiClient->request('invoice.list', $apiRequest);
        $clients = FreshbooksClient::all()->mapWithKeys(function ($client) {
            return [$client->freshbooks_ref => $client->id];
        })->toArray();

        if (is_null($invoices)) {
            return response()->json(['status' => 'error']);
        }

        $attributes = $invoices->attributes();

        if ($attributes->get('total') == 0) {
            return response()->json($attributes);
        }

        $invoices = $invoices->get('invoices')->invoice;

        if ($attributes->get('total') == 1) {
            $invoices = [$invoices];
        }

        $ignoredStatuses = [
            'draft',
        ];

        foreach ($invoices as $invoice) {
            if (in_array($invoice->status, $ignoredStatuses) ||
                !array_key_exists($invoice->client_id, $clients)) {
                continue;
            }

            $total = 0;
            $lines = [];
            $lineItems = $invoice->lines->line;

            if (!is_array($lineItems)) {
                $lineItems = [$lineItems];
            }

            foreach ($lineItems as $line) {
                $amount = floatval($line->amount);
                $total += $amount;

                if ($amount > 0) {
                    $lines[] = [
                        'sku' => is_object($line->name) ? 'Other' : $line->name,
                        'amount' => $amount * 100,
                    ];
                }
            }

            $currentInvoice = FreshbooksInvoice::updateOrCreate([
                'freshbooks_ref' => $invoice->invoice_id,
            ], [
                'client_id' => $clients[$invoice->client_id],
                'status' => $invoice->status,
                'invoice_date' => $invoice->date,
                'total' => $total * 100,
                'final' => floatval($invoice->amount) * 100,
            ]);

            $currentInvoice->lines()->delete();

            if (!empty($lines)) {
                $currentInvoice->lines()->createMany($lines);
            }
        }

        return response()->json($attributes);
    }
}
