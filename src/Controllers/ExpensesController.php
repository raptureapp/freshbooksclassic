<?php

namespace Rapture\FreshbooksClassic\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\FreshbooksClassic\Models\FreshbooksExpense;

class ExpensesController extends Controller
{
    public function index(Request $request)
    {
        $expenses = FreshbooksExpense::with('category')->get()->groupBy(function ($expense) {
            return $expense->posted_date->format('Y-m');
        })->map(function ($group) {
            return $group->groupBy(function ($expense) {
                return $expense->category->name;
            })->map(function ($category) {
                return $category->sum('amount');
            });
        })->sortKeysDesc();

        return view('freshbooks::expenses.index', compact('expenses'));
    }
}
