<?php

namespace Rapture\FreshbooksClassic\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rapture\FreshbooksClassic\Models\FreshbooksCategory;
use Rapture\FreshbooksClassic\Models\FreshbooksClient;
use Rapture\FreshbooksClassic\Models\FreshbooksExpense;
use Rapture\FreshbooksClassic\Models\FreshbooksInvoice;

class ImportController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('freshbooks.import', 'freshbooks');

        $lastRun = null;

        if ($request->filled('since')) {
            $lastRun = Carbon::parse($request->query('since'));
        } else {
            $latestCategory = FreshbooksCategory::orderBy('updated_at', 'desc')->first();
            $latestExpense = FreshbooksExpense::orderBy('posted_date', 'desc')->first();
            $latestClient = FreshbooksClient::orderBy('updated_at', 'desc')->first();
            $latestInvoice = FreshbooksInvoice::orderBy('invoice_date', 'desc')->first();

            $runTimes = [];

            if ($latestCategory) {
                $runTimes[] = $latestCategory->updated_at;
            }

            if ($latestExpense) {
                $runTimes[] = $latestExpense->posted_date;
            }

            if ($latestClient) {
                $runTimes[] = $latestClient->updated_at;
            }

            if ($latestInvoice) {
                $runTimes[] = $latestInvoice->invoice_date;
            }

            if (count($runTimes) === 4) {
                $lastRun = collect($runTimes)->sort()->pop();
            }
        }

        return view('freshbooks::import.index', compact('lastRun'));
    }
}
