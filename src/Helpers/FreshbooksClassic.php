<?php
namespace Rapture\FreshbooksClassic\Helpers;

use Rapture\FreshbooksClassic\Helpers\FreshbooksResponse;

class FreshbooksClassic
{
    private $line_ending = "\n";
    private $url;
    private $token;
    private $useragent;

    public function __construct()
    {
        $this->useragent = config('freshbooks.useragent');
        $this->token = config('freshbooks.token');
        $this->url = config('freshbooks.url');
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setURL($url)
    {
        $this->url = $url;
    }

    public function setUserAgent($agent)
    {
        $this->useragent = $agent;
    }

    /**
     * Process Request
     * @param string $method API Method
     * @param array $params Request
     * @return mixed Parsed Response
     */
    public function request($method, $params = '')
    {
        $requestBody = "<?phpxml version='1.0' encoding='utf-8'?>" . $this->line_ending;
        $requestBody .= "<request method='" . $method . "'>" . $this->line_ending;

        if (!empty($params)) {
            $requestBody .= $this->formatXML($params);
        }

        $requestBody .= "</request>";

        $curl = curl_init($this->url);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_NOBODY, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl, CURLOPT_USERPWD, $this->token);
        curl_setopt($curl, CURLOPT_TIMEOUT, 4);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_USERAGENT, $this->useragent);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);

        $curl_result = curl_exec($curl);

        curl_close($curl);

        if (strlen($curl_result) < 2) {
            // echo "Could not connect to FreshBooks server.";
            return null;
        }

        $response = new FreshbooksResponse(simplexml_load_string($curl_result, "SimpleXMLElement", LIBXML_NOCDATA));

        if ($response->status() == 'ok') {
            return $response;
        }

        return null;
    }

    /**
     * Properly Format XML
     * @param array $fields Array of parameters
     * @return string XML
     */
    private function formatXML($fields)
    {
        $xml = '';

        foreach ($fields as $tag => $field) {
            $xml .= '<' . $tag . '>';

            if (is_array($field)) {
                $xml .= $this->line_ending;
                $xml .= $this->formatXML($field);
            } else {
                $xml .= $field;
            }

            $xml .= '</' . $tag . '>' . $this->line_ending;
        }

        return $xml;
    }
}
