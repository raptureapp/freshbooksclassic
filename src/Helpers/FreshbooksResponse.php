<?php

namespace Rapture\FreshbooksClassic\Helpers;

class FreshbooksResponse
{
    protected $response;
    protected $data;
    protected $attributes;

    public function __construct($response)
    {
        $this->response = $response;
        $this->data = collect(json_decode(json_encode($response)));
        $this->attributes = collect($this->data->get('@attributes'));

        if ($this->data->count() > 1) {
            $this->attributes = $this->attributes->merge($this->data->pluck('@attributes')->filter()->first());
        }

        $this->data = $this->data->except('@attributes');
    }

    public function status()
    {
        return $this->attributes->get('status');
    }

    public function totalPages()
    {
        return intval($this->attributes->get('pages', 1));
    }

    public function currentPage()
    {
        return intval($this->attributes->get('page', 1));
    }

    public function nextPage()
    {
        return $this->currentPage() + 1;
    }

    public function perPage()
    {
        return intval($this->attributes->get('per_page', 25));
    }

    public function totalRecords()
    {
        return intval($this->attributes->get('total', 1));
    }

    public function body()
    {
        return $this->data;
    }

    public function get($key)
    {
        return $this->data->get($key);
    }

    public function attribute($key)
    {
        return $this->attributes->get($key);
    }

    public function attributes()
    {
        return $this->attributes;
    }
}
