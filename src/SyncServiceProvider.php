<?php

namespace Rapture\FreshbooksClassic;

use Rapture\Core\Installer;
use Rapture\Core\Models\Menu;
use Rapture\FreshbooksClassic\Listeners\DisplayExistingSyncFields;
use Rapture\FreshbooksClassic\Listeners\DisplayNewSyncFields;
use Rapture\Hooks\Facades\Hook;
use Rapture\Packages\Providers\PackageProvider;

class SyncServiceProvider extends PackageProvider
{
    use Installer;

    public function contents()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'freshbooks');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'freshbooks');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        Hook::attach('clients.create.company', function ($data) {
            echo view('freshbooks::clients.company');
        });

        Hook::attach('clients.create.sidebar', DisplayNewSyncFields::class);
        Hook::attach('clients.edit.sidebar', DisplayExistingSyncFields::class);

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/freshbooks.php' => config_path('freshbooks.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../public' => public_path('freshbooks'),
            ], 'rapture');
        }
    }

    public function install()
    {
        $this->registerPermission('freshbooks', 'sales', 'freshbooks::package.sales');
        $this->registerPermission('freshbooks', 'import', 'freshbooks::package.import');

        $clientsMenu = Menu::where([
            'package' => 'rapture/clients',
            'placement' => 'root',
            'menu_id' => null,
        ])->first();

        $this->addMenu([
            'menu_id' => $clientsMenu->id,
            'label' => 'freshbooks::package.sales',
            'route' => 'dashboard.freshbooks.sales',
            'namespaces' => ['dashboard/freshbooks/sales'],
            'permission' => 'freshbooks.sales'
        ]);

        $this->addMenu([
            'menu_id' => $clientsMenu->id,
            'label' => 'freshbooks::package.import',
            'route' => 'dashboard.freshbooks.import',
            'namespaces' => ['dashboard/freshbooks/import'],
            'permission' => 'freshbooks.import'
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/freshbooks.php', 'freshbooks');
    }
}
