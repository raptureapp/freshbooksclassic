<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreshbooksExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freshbooks_expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('freshbooks_category_id')->index();
            $table->string('ref')->index();
            $table->integer('amount');
            $table->string('posted_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freshbooks_expenses');
    }
}
