# Freshbooks Classic

Add the following records to your .env file

FRESHBOOKS_TOKEN  
FRESHBOOKS_USER_AGENT (Optional; Defaults to 'Rapture')
FRESHBOOKS_URL
